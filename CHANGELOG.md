## [1.6.2] - 2023-04-05
### Fixed
- bottom margin

## [1.6.1] - 2022-08-30
### Fixed
- de_DE translators

## [1.6.0] - 2022-08-16
### Added
- en_CA, en_GB translators

## [1.5.0] - 2022-08-16
### Added
- de_DE translators

## [1.4.0] - 2022-08-09
### Added
- en_AU translators

## [1.3.1] - 2022-07-13
### Fixed
- text domain

## [1.3.0] - 2022-05-22
### Changes
- plugin flow library

## [1.2.1] - 2022-02-07
### Changes
- Styling

## [1.2.0] - 2022-01-24
### Changes
- Added TextPetition

## [1.1.0] - 2019-12-02
### Changes
- RatingPetitionNotice can get url to redirect user when wants to rate

## [1.0.0] - 2019-11-29
### Added
- First version