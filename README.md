[![pipeline status](https://gitlab.com/wpdesk/wp-wpdesk-rating-petition/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-wpdesk-rating-petition/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-rating-petition/badges/master/coverage.svg)](https://gitlab.com/wpdesk/wp-wpdesk-rating-petition/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-wpdesk-rating-petition/v/stable)](https://packagist.org/packages/wpdesk/wp-wpdesk-rating-petition) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-wpdesk-rating-petition/downloads)](https://packagist.org/packages/wpdesk/wp-wpdesk-rating-petition) 
[![License](https://poser.pugx.org/wpdesk/wp-wpdesk-rating-petition/license)](https://packagist.org/packages/wpdesk/wp-wpdesk-rating-petition)

# Rating Petition

A WordPress library which can be used to encourage the plugin users to review and rate it in WordPress repository.

## Requirements

PHP 7.0 or later.

## Installation via Composer

In order to install the bindings via [Composer](http://getcomposer.org/) run the following command:

```bash
composer require wpdesk/wp-wpdesk-rating-petition
```

## Example usage

### To use it in the shipping zones area:

```php
function init_repository_rating() {
    $time_tracker = new \WPDesk\RepositoryRating\TimeWatcher\ShippingMethodInstanceWatcher(
        FedexShippingService::UNIQUE_ID,
        'plugin_activation_flexible-shipping-fedex/flexible-shipping-fedex.php',
        '28-11-2019',
        FedexShippingMethod::class
    );
    $time_tracker->hooks();
    ( new \WPDesk\RepositoryRating\RatingPetitionNotice(
        $time_tracker,
        'flexible_shipping_fedex',
        $this->plugin_info->get_plugin_name(),
        'https://wpde.sk/fs-fedex-rate'
    ) )->hooks();
}
```

### To use it on the shipping method screen:

```php
<?php
function init_repository_rating() {
    $time_tracker = new \WPDesk\RepositoryRating\TimeWatcher\ShippingMethodGlobalSettingsWatcher(
        'flexible_shipping_fedex'
    );
    $time_tracker->hooks();
    ( new \WPDesk\RepositoryRating\RatingPetitionNotice(
            $time_tracker,
            'flexible_shipping_fedex',
            $this->plugin_info->get_plugin_name(),
            'https://wpde.sk/fs-fedex-rate'
        )
    )->hooks();
}
```
